﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class MemberDAO
    {
        public static List<Member> GetMembers()
        {
            var listMembers = new List<Member>();
            try
            {
                using (var context = new MyDBContext())
                {
                    listMembers = context.Members.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listMembers;
        }
        public static Member FindMemberById(int memberId)
        {
            var member = new Member();
            try
            {
                using (var context = new MyDBContext())
                {
                    member = context.Members.SingleOrDefault(c => c.MemberId == memberId);
                    if (member == null)
                    {
                        throw new Exception("Member does not exist"); // Ném ngoại lệ khi không tìm thấy sản phẩm
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return member;
        }
        public static void SaveMember(Member member)
        {
            try
            {
                using (var context = new MyDBContext())
                {
                    context.Members.Add(member);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void UpdateMember(Member member)
        {
            try
            {
                using (var context = new MyDBContext())
                {
                    context.Entry(member).State =
                        Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void DeleteMember(Member member)
        {
            try
            {
                using (var context = new MyDBContext())
                {
                    var customerToDelete = context
                        .Members
                        .SingleOrDefault(c => c.MemberId == member.MemberId);
                    context.Members.Remove(customerToDelete);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
