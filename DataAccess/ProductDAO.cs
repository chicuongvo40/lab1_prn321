﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ProductDAO
    {
        public static List<Product> GetProducts()
        {
            var listProducts = new List<Product>();
            try
            {
                using (var context = new MyDBContext()
                )
                {
                    listProducts.AddRange( context.Products );
                } ;
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return listProducts;
        }
        public static Product FindProductById(int proId)
        {
            Product p = null;

            try
            {
                using (var context = new MyDBContext())
                {
                    p = context.Products.SingleOrDefault(x => x.ProductId == proId);
                    if (p == null)
                    {
                        throw new Exception("Product does not exist"); // Ném ngoại lệ khi không tìm thấy sản phẩm
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return p;
        }


        public static List<Product> Search(string keyword)
        {
            var listProduct = new List<Product>();
            try
            {
                using (var context = new MyDBContext())
                {
                    // Tìm tất cả sản phẩm có ProductName chứa keyword
                    listProduct = context.Products.Where(f => f.ProductName.Contains(keyword)).ToList();

                    // Tìm tất cả OrderDetails có UnitPrice tương tự với sản phẩm
                    foreach (var product in listProduct)
                    {
                        product.OrderDetails = context.OrderDetails.Where(od => od.UnitPrice == product.UnitPrice).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listProduct;
        }

        public static void SaveProduct(Product p)
        {
            try
            {
                using (var context = new MyDBContext())
                {
                    context.Products.Add(p);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static void UpdateProduct(Product p)
        {
            try
            {
                using (var context = new MyDBContext())
                {
                    context.Entry<Product>(p).State =Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static Product DeleteProduct(Product p)
        {
            try
            {
                using (var context = new MyDBContext())
                {
                   var p1 = context.Products.SingleOrDefault(c => c.ProductId == p.ProductId);
                    context.Products.Remove(p1);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return p;
        }
    }
}
