﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class MemberRequest
    {
        public MemberRequest()
        {
        }

        public MemberRequest(int memberId, string email, string companyName, string city, string country, string? password)
        {
            MemberId = memberId;
            Email = email;
            CompanyName = companyName;
            City = city;
            Country = country;
            Password = password;
        }

        public int MemberId { get; set; }
        [Required, StringLength(40)]
        public string Email { get; set; }
        [Required, StringLength(40)]
        public string CompanyName { get; set; }
        [Required, StringLength(40)]
        public string City { get; set; }
        [Required, StringLength(40)]
        public string Country { get; set; }
        [MinLength(5)]
        [MaxLength(255)]
        public string? Password { get; set; }
    }
}
