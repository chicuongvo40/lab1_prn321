﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    public  class Product
    {
        public Product(int productId, string productName, string weight, int categoryId, int unitsInStock, decimal unitPrice)
        {
            ProductId = productId;
            ProductName = productName;
            Weight = weight;
            CategoryId = categoryId;
            UnitsInStock = unitsInStock;
            UnitPrice = unitPrice;
 
        }
        public Product()
        {

        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }
        [Required, StringLength(40)]
        public string ProductName { get; set; }
        [Required, StringLength(40)]
        public string Weight { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int UnitsInStock  { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        public virtual Category Categorys { get;}
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
